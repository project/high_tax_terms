# Highlight taxonomy terms

## INTRODUCTION

Provide textfilter to highlight taxonomy terms.

## REQUIREMENTS

This module requires the following modules:
 * Filter (Core)
 * Taxonomy (Core)

## INSTALLATION

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-8
for further information.
