(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.high_tax_terms = {
    attach: function (context, settings) {
        $('.terms_replace').hover(function () {
          $(this).find('.terms_replace_description').show('fast');
        }, function () {
          $(this).find('.terms_replace_description').hide('fast');
        });
        $('.terms_replace_description').hover(function () {
          $(this).hide();
        });
    }
  };
})(jQuery, Drupal);

