<?php

namespace Drupal\high_tax_terms\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Provide textfilter to highlight taxonomy terms.
 *
 * @Filter(
 *   id = "filter_high_tax_terms",
 *   title = @Translation("Highlight taxonomy terms"),
 *   description = @Translation("Provide textfilter to highlight taxonomy terms"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterHighTaxTerms extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->settings['high_tax_terms_vocabolary']);

    foreach ($terms as $term) {
      if (isset($this->settings['high_tax_terms_solo']) && $this->settings['high_tax_terms_solo'] == 1) {
        $pattern = '/\b' . $term->name . '\b/';
      }
      else {
        $pattern = '/' . $term->name . '/';
      }

      if (!isset($this->settings['high_tax_terms_case_sesitive']) || $this->settings['high_tax_terms_case_sesitive'] == 0) {
        $pattern .= 'i';
      }

      $replacement = [
        '#theme' => 'high-tax-terms',
        '#tax_title' => $term->name,
        '#tax_description' => strip_tags($term->description__value)
      ];
      $text = preg_replace($pattern, \Drupal::service('renderer')->renderPlain($replacement), $text);
/*
      switch (TRUE) {
        case (isset($term->description__value)):
          $replacement = '<span class="ttf_replace">$0<span class="ttf_description">' . strip_tags($term->description__value) . '</span></span>';
          $text = preg_replace($pattern, $replacement, $text);
          break;

        case (!isset($term->description__value)):
          $replacement = '<span class="ttf_replace_none">$0</span>';
          $text = preg_replace($pattern, $replacement, $text); 
          break;
      }
      */
    }

    $result = new FilterProcessResult($text);
    $result->setAttachments([
      'library' => [
        'high_tax_terms/high_tax_terms_popup',
      ],
    ]);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = [];
    foreach (Vocabulary::loadMultiple() as $vocs) {
      $options[$vocs->id()] = $vocs->label();
    }

    $default_case_sesitive = isset($this->settings['high_tax_terms_case_sesitive']) ? $this->settings['high_tax_terms_case_sesitive'] : 1;
    $default_stand_alone = isset($this->settings['high_tax_terms_solo']) ? $this->settings['high_tax_terms_solo'] : 1;

    $form['high_tax_terms_vocabolary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $options,
      '#default_value' => $this->settings['high_tax_terms_vocabolary'],
      '#description' => $this->t('Choose a vocabulary to highlight the taxonomies it contains.'),
    ];
    $form['high_tax_terms_case_sesitive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Case sensitive'),
      '#default_value' => $default_case_sesitive,
    ];
    $form['high_tax_terms_solo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Stand alone'),
      '#default_value' => $default_stand_alone,
      '#description' => $this->t('Taxonomy term must stand alone in the text.<br><i>(Not part of any other word)</i>'),
    ];

    return $form;
  }

}
